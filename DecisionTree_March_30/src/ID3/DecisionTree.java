package ID3;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import team4.CategoryRules.MultipulSplit;
import team4.bean.Attribute;
import team4.bean.Rules;
import team4.bean.TestData;
import team4.bean.TrainData;
import team4.fileIO.FileConvert;
import team4.fileIO.FileRead;

public class DecisionTree {
	Node Root;

	public String predict(TestData d) {
		String label = predict(d, Root);
		return label;
	}

	private String predict(TestData d, Node node) {
		if (node == null) {
			return "No predict";
		}

		if (node.label != null && node.label.length() != 0) {
			return node.label;
		}
		int index = node.getBranch(d);
		return predict(d, node.next[index]);

	}

	private Node ID3(List<TrainData> Examples, List<Attribute> Attributes) {
		// create a root node for the tree
		Node root = new Node();
		String mostLabel = TrainData.mostLabel(Examples);

		// if all examples are in the same label, give the root this label and
		// return root
		if (TrainData.isLabelSame(Examples)) {
			root.label = TrainData.mostLabel(Examples);
			return root;
		}

		// if Attributes is empty, return the root, root label is the most label
		if (Attributes == null || Attributes.size() == 0) {
			root.label = mostLabel;
			return root;
		}

		// A <- the attribute form Attributes best classifies the Examples
		Attribute A = Attributes.get(0);
		// The decision attribute for Root <- A
		root = new Node(A);

		// separate the Examples by A's different values
		for (int i = 0; i < A.getChoices().length; i++) {
			String val = A.getChoices()[i];
			List<TrainData> list = TrainData.getDataWithAttribute(Examples,
					val, A.getPos());
			// if list == null, add a leaf node with most label of Examples
			if (list == null || list.size() == 0) {
				Node leaf = new Node();
				leaf.label = mostLabel;
				root.next[i] = leaf;
			} else {
				// copy Attributes
				List<Attribute> Attributes_left = new LinkedList<Attribute>(
						Attributes);
				Attributes_left.remove(0);
				// root .next <- ID3(list,Attributes_left)
				root.next[i] = ID3(list, Attributes_left);
			}

		}

		return root;
	}

	private static class Node {
		private Attribute attribute;
		private Node[] next;
		private String label;

		@Override
		public String toString() {
			if (attribute != null) {
				return attribute.toString() + " label:" + label;
			}
			return "label:" + label;

		}

		public Node() {

		}

		public Node(Attribute a) {
			attribute = a;
			next = new Node[a.nodeBranchNum()];
		}

		public int getBranch(TestData d) {
			int pos = attribute.getPos();
			String da = d.getAttributes()[pos];
			return attribute.getBranch(da);
		}
	}

	public void constructTree(List<TrainData> rawD, String attributesFile,
			Attribute CLASS, List<Rules> rules) {
		// convert
		FileConvert<TrainData> fc = new FileConvert<TrainData>();
		List<TrainData> Examples = fc.changeToCategory(rules, rawD); // success

		// System.out.println(Examples);

		// get all attributes, sort by gain
		FileRead<Attribute> fd2 = new FileRead<Attribute>();
		List<Attribute> Attributes = fd2.readToClass(attributesFile, "",
				new Attribute());
		// compute all the Attribute
		for (Attribute a : Attributes) {
			a.computeGain(Examples, CLASS);
		}
		Collections.sort(Attributes);
		// System.out.println(Attributes);
		this.Root = ID3(Examples, Attributes);
	}
	
	public DecisionTree(List<TrainData> rawD, String attributesFile,
			Attribute CLASS, List<Rules> rules){
		constructTree(rawD, attributesFile, CLASS, rules);
	}
	

	public DecisionTree(String trainFile, String attributesFile,
			Attribute CLASS, List<Rules> rules) {
		String starter = "@data";
		// get all examples, train data
		// read raw data
		FileRead<TrainData> fd = new FileRead<TrainData>();
		List<TrainData> rawD = fd.readToClass(trainFile, starter,
				new TrainData());

		constructTree(rawD, attributesFile, CLASS, rules);
	}

	public static void main(String[] args) {

	}
}
