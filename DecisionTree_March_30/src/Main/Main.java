package Main;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import team4.CategoryRules.MultipulSplit;
import team4.bean.Attribute;
import team4.bean.Rules;
import team4.bean.TestData;
import team4.bean.TrainData;
import team4.fileIO.FileConvert;
import team4.fileIO.FileRead;
import ID3.DecisionTree;
import ID3.SplitMethod;

/*
 * Run cross Validation  java Main.java CV A/B 
 * precit result java 
 * Main.java predict trainProdIntro.binary.arff part_B_Attributtes.txt testProdIntro.binary.arff notNum,Label,8,0,1 1
 * 
 */



public class Main {

	public static void main(String[] args) {
		String trainDataFile;
		String attributesFile;
		String testFile;
		String targetAttribute;
		


		// predict(trainDataFile, attributesFile, testFile, targetAttribute);
		if (args.length == 6 && args[0].equals("predict")) {
			List<TestData> list = predict(args[1], args[2], args[3], args[4],
					Integer.parseInt(args[5]), "@data");
			for(TestData td : list){
				System.out.println(td.getLabel());
			}
			
			
			// predict trainProdIntro.binary.arff part_B_Attributtes.txt testProdIntro.binary.arff notNum,Label,8,0,1 1
		} else if (args.length == 2 && args[0].equals("CV")) {
			// CV A
			if (args[1].equals("A")) {
				trainDataFile = "trainProdSelection.arff";
				attributesFile = "part_A_Attributtes.txt";
				testFile = "testProdSelection.arff";
				targetAttribute = "notNum,Label,6,C1,C2,C3,C4,C5";
				crossValidation(trainDataFile, attributesFile, testFile,
						targetAttribute, 2);
			} else if (args[1].equals("B")) {
				trainDataFile = "trainProdIntro.binary.arff";
				attributesFile = "part_B_Attributtes.txt";
				testFile = "testProdIntro.binary.arff";
				targetAttribute = "notNum,Label,8,0,1";
				crossValidation(trainDataFile, attributesFile, testFile,
						targetAttribute, 1);
			}
		}
	}

	public static void crossValidation(String trainDataFile,
			String attributesFile, String testFile, String targetAttribute,
			int num) {
		// init

		int[][] rate = new int[10][2];

		// read from the trainDataFile
		FileRead<TrainData> fr = new FileRead<TrainData>();
		List<TrainData> allData = fr.readToClass(trainDataFile, "@data",
				new TrainData());
		// shuffle the data
		Collections.shuffle(allData);

		// give a for loop of 10 times
		for (int i = 0; i < 10; i++) {
			int len = allData.size() / 10;
			int lo = i * len;
			int hi = (i + 1) * len;
			// choice the i part as test, others as train
			List<TrainData> trainList = new LinkedList<TrainData>();
			List<TestData> predictList = toTestData(allData.subList(lo, hi));
			trainList.addAll(allData.subList(0, lo));
			trainList.addAll(allData.subList(hi, allData.size()));

			// execute predict
			List<TestData> testList = predict(trainList, attributesFile,
					predictList, targetAttribute, num);
			// compute the accuracy
			rate[i] = accuracy(testList);
		}

		// combine the overall accuracy
		double right = 0, sum = 0;
		for (int i = 0; i < rate.length; i++) {
			right += rate[i][0];
			sum += rate[i][1];
		}

		double accuracy = right / sum;
		System.out.println("cossValidation accuracy : " + accuracy);
	}

	private static List<TestData> toTestData(List<TrainData> list) {
		List<TestData> result = new LinkedList<TestData>();
		for (TrainData td : list) {
			TestData test = new TestData(td);
			result.add(test);
		}
		return result;
	}

	private static int[] accuracy(List<TestData> testList) {
		//System.out.println(testList);
		int[] ans = new int[2];
		if (testList == null) {
			return ans;
		}
		ans[1] = testList.size();
		for (TestData td : testList) {
			if (td.rightPredict()) {
				ans[0]++;
			}
		}
		return ans;
	}

	public static List<TestData> predict(List<TrainData> trainList,
			String attributesFile, List<TestData> testList,
			String targetAttribute, int catNum) {
		Attribute CLASS = (Attribute) new Attribute()
				.updateByString(targetAttribute);

		// get rules
		SplitMethod splitMethod = new MultipulSplit(CLASS, catNum);
		List<Rules> rules = splitMethod.returnRules(trainList, attributesFile);

		DecisionTree t = new DecisionTree(trainList, attributesFile, CLASS,
				rules);
		FileConvert<TestData> fc = new FileConvert<TestData>();
		List<TestData> testList2 = fc.changeToCategory(rules, testList);
		for (TestData td : testList2) {
			td.setLabel(t.predict(td));
		}
		return testList2;
	}

	public static List<TestData> predict(String trainDataFile,
			String attributesFile, String testFile, String targetAttribute,
			int catNum, String starter) {
		Attribute CLASS = (Attribute) new Attribute()
				.updateByString(targetAttribute);

		// get rules
		SplitMethod splitMethod = new MultipulSplit(CLASS, catNum);
		List<Rules> rules = splitMethod.returnRules(trainDataFile,
				attributesFile);

		// build tree
		DecisionTree t = new DecisionTree(trainDataFile, attributesFile, CLASS,
				rules);

		// use the rules to convert predict data
		FileRead<TestData> fr = new FileRead<TestData>();
		List<TestData> testList = fr.readToClass(testFile, starter,
				new TestData());
		FileConvert<TestData> fc = new FileConvert<TestData>();
		List<TestData> testList2 = fc.changeToCategory(rules, testList);

		for (TestData td : testList2) {
			td.setLabel(t.predict(td));
		}
		return testList2;
	}

}
