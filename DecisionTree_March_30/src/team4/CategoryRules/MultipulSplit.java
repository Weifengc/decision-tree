package team4.CategoryRules;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import team4.bean.Attribute;
import team4.bean.CompareByPos;
import team4.bean.Rules;
import team4.bean.TrainData;
import team4.fileIO.FileConvert;
import team4.fileIO.FileRead;
import ID3.SplitMethod;
import MultiSplit.FindCombo;

public class MultipulSplit implements SplitMethod {
	/*
	 * according to the result num to calculate the spliter numbasic idea,sort
	 * the attributes, put int to set so that delete the duplicateput the set to
	 * a list, sort again,new a list, contians the middle of all the listfind
	 * combonation of that listcompute the information gain by all combination
	 */
	public static void main(String[] args) {

		String trainDataFile = "trainProdSelection.arff";
		String attributesFile = "part_A_Attributtes.txt";
		String line2 = "notNum,Label,6,C1,C2,C3,C4,C5";

		Attribute CLASS = (Attribute) new Attribute().updateByString(line2);

		MultipulSplit ms = new MultipulSplit(CLASS, 3);
		ms.returnRules(trainDataFile, attributesFile);

	}

	public void setSpliterNum(int num) {
		this.spliterNum = num;
	}

	private int spliterNum;
	private Attribute CLASS;

	public MultipulSplit(Attribute CLASS) {
		this.CLASS = CLASS;
		spliterNum = CLASS.getChoices().length - 1;
	}

	public MultipulSplit(Attribute CLASS, int num) {
		this.CLASS = CLASS;
		spliterNum = num;
	}

	@Override
	public List<Rules> returnRules(String trainDataFile, String attributesFile) {

		// read train data and attibutes
		FileRead<TrainData> fr = new FileRead<TrainData>();
		List<TrainData> dataList = fr.readToClass(trainDataFile, "@data",
				new TrainData());
		
		return returnRules(dataList, attributesFile);

	}

	@Override
	public List<Rules> returnRules(List<TrainData> dataList,
			String attributesFile) {

		List<Attribute> attributeList = numAttributeList(attributesFile);
		// init
		List<Rules> rules = new LinkedList<Rules>();

		// for every attribute find its best info gain rules
		for (Attribute a : attributeList) {
			rules.addAll(maxGainRules(a, dataList));
		}
		return rules;
	}

	// find the rules make the attribute to have the max info gain
	private List<Rules> maxGainRules(Attribute a, List<TrainData> dataList) {
		// prepocessing
		// sort the dataList by this attribute
		// System.out.println("computing the max rules of :" + a.getName());
		Comparator c = new CompareByPos(a);
		Collections.sort(dataList, c);

		// put the list to a set
		Set<Double> set = new HashSet<Double>();
		for (TrainData td : dataList) {
			double d = Double.parseDouble(td.getAttributes()[a.getPos()]);
			set.add(d);
		}

		// put set to another list and sort
		List<Double> sorted = new LinkedList<Double>();
		for (Double d : set) {
			sorted.add(d);
		}
		Collections.sort(sorted);
		// clear set
		set = null;

		// find the middle of all the sorted
		List<Double> midList = new LinkedList<Double>();
		for (int i = 0; i < sorted.size() - 1; i++) {
			double d = sorted.get(i) + (sorted.get(i + 1) - sorted.get(i)) / 2;
			midList.add(d);
		}
		// clear sorted
		sorted = null;

		// try all combination, compute the gain

		List<List<Double>> allCombination = FindCombo
				.findCombinationFromSortedList(midList, this.spliterNum);
		// clear midList
		midList = null;

		// loop through allCombination, create all posible rules

		List<Rules> bestRule = null;
		double maxGain = Double.MIN_VALUE;

		for (List<Double> combo : allCombination) {

			List<Rules> rule = createRule(combo, a);
			FileConvert<TrainData> fc = new FileConvert<TrainData>();
			List<TrainData> converted = fc.changeToCategory(rule, dataList);
			if (a.computeGain(converted, this.CLASS) > maxGain) {
				bestRule = rule;
				maxGain = a.getGain();
			}

		}

		// System.out.println(a.getName() + "rule is as following");
		// System.out.println(a.getName() +
		// " Attribute main rules complete, info gain:"+maxGain);
		// System.out.println(bestRule);
		return bestRule;
	}

	private List<Rules> findBestRule(List<List<Rules>> rules, Attribute a,
			List<TrainData> dataList) {
		// System.out.println("possible rules num:" + rules.size());

		List<Rules> bestRules = null;
		double maxGain = 0;
		for (List<Rules> r : rules) {
			// change the
			FileConvert<TrainData> fc = new FileConvert<TrainData>();
			List<TrainData> converted = fc.changeToCategory(r, dataList);
			if (a.computeGain(converted, this.CLASS) > maxGain) {
				bestRules = r;
				maxGain = a.getGain();
			}
		}
		return bestRules;
	}

	private List<Rules> createRule(List<Double> combo, Attribute a) {
		List<String> newLabels = new LinkedList<String>();
		for (int i = 0; i < this.spliterNum + 1; i++) {
			newLabels.add("" + i);
		}
		List<String> spli = new LinkedList<String>();
		spli.add("" + Integer.MIN_VALUE);
		for (int i = 0; i < combo.size(); i++) {
			spli.add("" + combo.get(i));
		}
		spli.add("" + Integer.MAX_VALUE);

		List<Rules> rules = new LinkedList<Rules>();
		for (int i = 0; i < this.spliterNum + 1; i++) {
			String[] ss = { "" + a.getPos(), spli.get(i), spli.get(i + 1),
					newLabels.get(i) };
			Rules r = new Rules(ss);
			rules.add(r);
		}
		return rules;
	}

	// find all attirbute that is num
	private List<Attribute> numAttributeList(String fileName) {
		String[] choices = new String[this.spliterNum + 1];
		for (int i = 0; i < choices.length; i++) {
			choices[i] = "" + i;
		}

		// read all from flieName
		FileRead<Attribute> fr = new FileRead<Attribute>();
		List<Attribute> attList = fr.readToClass(fileName, "", new Attribute());

		List<Attribute> resultList = new LinkedList<Attribute>();
		// loop through, delete the not num attribute
		for (Attribute a : attList) {
			if (a.isNum()) {
				a.setChoices(choices);
				resultList.add(a);
			}
		}
		return resultList;
	}

}
