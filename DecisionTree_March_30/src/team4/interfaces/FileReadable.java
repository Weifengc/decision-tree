package team4.interfaces;

public interface FileReadable {
	public FileReadable updateByString(String input);
}
