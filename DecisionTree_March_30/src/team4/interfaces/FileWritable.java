package team4.interfaces;

public interface FileWritable {
	public String writeToFile();// return the string of a data, used to write to file
}
