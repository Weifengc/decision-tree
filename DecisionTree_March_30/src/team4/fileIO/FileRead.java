package team4.fileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import team4.bean.TrainData;
import team4.interfaces.FileReadable;

public class FileRead<T> {

	// read a file from line to line, construct a list of String
	public static List<String> readAll(String fileName){
		List<String> lines = new LinkedList<String>();
		try {
			Scanner scanner = new Scanner(new File(fileName));
			while(scanner.hasNextLine()){
				lines.add(scanner.nextLine());
			}
		} catch (FileNotFoundException e) {
			System.out.println("Did Not find File"+ fileName);
		}
		return lines;
	}
	
	//read a file from some point
	public static List<String> readFrom(String fileName, String starter){
		if(starter == null || starter.equals("")){
			return readAll(fileName);
		}
		
		List<String> fileLines = readAll(fileName);
		if(fileLines == null || fileLines.size() == 0){
			return null;
		}
		int pos = fileLines.indexOf(starter);
		if(pos < 0){
			return null;
		}
		List<String> result = new LinkedList<String>();
		for(int i = pos+1 ; i < fileLines.size(); i++){
			result.add(fileLines.get(i));
		}
		return result;
	}
	
	
	//read a file and make it to be a list of class
	//use interface, FileReadable
	public List<T> readToClass(List<String> lines, FileReadable fd){
		if(lines == null){
			return null;
		}
		//create a list of T
		List<T> returnList = new LinkedList<T>();
		for(String s : lines){
			returnList.add((T)fd.updateByString(s));
		}
		return returnList;
	}

	public List<T> readToClass(String fileName,  String starter,FileReadable fd ){
		List<String> lines = readFrom(fileName, starter);
		return readToClass(lines,fd);
	}
	
	public static void main(String[] args){
		//test method
		String file  = "trainProdSelection.arff";
//		System.out.println(readAll(file)); //success
//		System.out.println(readFrom(file,"@data")); //success
		
		FileRead<TrainData> fd = new FileRead<TrainData>();
//		System.out.println(fd.readToClass(file,"@data",new TrainData())); // success
		
	}
	
}
