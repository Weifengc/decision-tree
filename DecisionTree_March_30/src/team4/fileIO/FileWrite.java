package team4.fileIO;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import team4.bean.Data;
import team4.bean.TrainData;
import team4.interfaces.FileWritable;

public class FileWrite<T extends Data>  {
	
	//Write a list of data to the file
	public boolean writeToFile(String fileName, List<T>list){
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName, "UTF-8");
			for(FileWritable fw : list){
				writer.println(fw.writeToFile());
			}
			writer.close();
			return true;
		} catch (FileNotFoundException e) {
			System.out.println("File Not found: "+fileName);
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return false;
	}

	
	public static void main(String[] args){
		String dataFile = "trainProdSelection.arff";
		String ruleFile = "part_A_Rules.txt";
		String starter = "@data";
		String targetFile = "target.txt";
		
		//read raw data	
		FileRead<TrainData> fd = new FileRead<TrainData>();
		List<TrainData> rawD = fd.readToClass(dataFile,starter,new TrainData());
		
		//convert
		FileConvert<TrainData> fc = new FileConvert<TrainData>();
		List<TrainData> convertedList = fc.changeToCategory(ruleFile,rawD); // this have bug
		new FileWrite<TrainData>().writeToFile(targetFile,rawD);
		
	}
}
