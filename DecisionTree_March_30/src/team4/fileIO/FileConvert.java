package team4.fileIO;

import java.util.LinkedList;
import java.util.List;

import team4.bean.Data;
import team4.bean.Rules;
import team4.bean.TrainData;
import team4.interfaces.CategoryAble;

public class FileConvert<T extends Data> {
	
	//change a list of data to be categoried
	public List<T> changeToCategory(String fileName, List<T> sourceList){
		//get rules
		FileRead<Rules> fd = new FileRead<Rules>();
		List<Rules> rules = fd.readToClass(fileName, "", new Rules());
		return changeToCategory(rules,sourceList);
	}
	
	public List<T> changeToCategory(List<Rules> rules, List<T> sourceList){
		//creaet a list of the same length
		List<T> resultList = new LinkedList<T>();
		for(CategoryAble c :  sourceList){
			resultList.add((T)c.changeToCategory(rules));
		}
		return resultList;
	}
	
	
	
	//test
	public static void main(String[] args){
		String dataFile = "trainProdSelection.arff";
		String ruleFile = "part_A_Rules.txt";
		String starter = "@data";
		
		//read raw data	
		FileRead<TrainData> fd = new FileRead<TrainData>();
		List<TrainData> rawD = fd.readToClass(dataFile,starter,new TrainData());
		System.out.println(rawD);
		
		//convert
		FileConvert<TrainData> fc = new FileConvert<TrainData>();
		List<TrainData> convertedList = fc.changeToCategory(ruleFile,rawD); // success
		System.out.println(convertedList);
	}
	
}
