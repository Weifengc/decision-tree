package team4.bean;

import java.util.Arrays;
import java.util.List;

import team4.fileIO.FileRead;
import team4.interfaces.CategoryAble;
import team4.interfaces.FileReadable;
import team4.interfaces.FileWritable;

public abstract class Data implements FileReadable, CategoryAble, FileWritable {
	protected String[] attributes;
	protected String label;

	public String[] getAttributes() {
		return attributes;
	}

	public void setAttributes(String[] attributes) {
		this.attributes = attributes;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	protected abstract Data copy();

	@Override
	public CategoryAble changeToCategory(String fileName) {
		List<Rules> rules = getRules(fileName);
		return changeToCategory(rules);
	}

	public CategoryAble changeToCategory(List<Rules> rules) {
		// go through its own attribute , return a new object
		Data d = this.copy();
		int prePos = -1;
		for (Rules r : rules) {
			int pos = r.pos;
			if (pos == prePos) {
				continue;
			} else {
				if (d.changeAttribute(r)) {
					prePos = pos;
				}
			}
		}
		return d;
	}

	private boolean changeAttribute(Rules r) {
		double num = Double.parseDouble(this.attributes[r.pos]);
		if (num >= r.start && num < r.end) {
			this.attributes[r.pos] = r.cat;
			return true;
		}
		return false;
	}

	public List<Rules> getRules(String fileName) {
		FileRead<Rules> fd = new FileRead<Rules>();
		List<Rules> rules = fd.readToClass(fileName, "", new Rules());
		return rules;
	}

	@Override
	public String toString() {
		return this.getClass() + " | " + "ATTRIBUTES:"
				+ Arrays.toString(this.attributes) + " | LABEL: " + label;
	}

	public static void main(String[] args) {
		String[] input = { "2", "2" };
		TestData td = new TestData(input);
		System.out.println(td.changeToCategory("rules.txt"));

	}

	@Override
	public String writeToFile() {
		// return all the attributes and label
		StringBuilder sb = new StringBuilder();
		for (String s : this.attributes) {
			sb.append(s);
			sb.append(",");
		}
		sb.append(this.label);
		return sb.toString();
	}

}
