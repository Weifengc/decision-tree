package team4.bean;

import team4.interfaces.FileReadable;


public class TestData extends Data{

	public TestData(){
		
	}
	public TestData(TrainData t){
		int len = t.getAttributes().length + 1;
		this.attributes = new String[len];
		System.arraycopy(t.getAttributes(),0,this.attributes,0,len -1);
		this.attributes[len - 1] = t.getLabel();
	}
	
	public TestData(String[] input){
		this.attributes = input;
	}

	public TestData(String input){
		this.attributes = input.split(",");
	}
	
	@Override
	public FileReadable updateByString(String input) {
		String[] ss = input.split(",");
		return new TestData(ss);
	}

	@Override
	protected Data copy() {
		return new TestData(this.attributes);
	}


	public boolean rightPredict(){
		return this.attributes[attributes.length - 1].equals(this.label);
	}
	
}
