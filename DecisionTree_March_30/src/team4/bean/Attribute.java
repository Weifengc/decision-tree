package team4.bean;

import java.util.Arrays;
import java.util.List;

import team4.fileIO.FileRead;
import team4.interfaces.FileReadable;

public class Attribute extends Data implements Comparable {
	private String name;
	private int pos;
	private String[] choices;
	private double gain;
	private boolean isNum;
	public String[] getChoices(){
		return choices;
	}

	public boolean isNum(){
		return isNum;
	}
	
	public Attribute() {

	}

	public void setChoices(String[] ss){
		choices = new String[ss.length];
		System.arraycopy(ss, 0, choices, 0, ss.length);
	}
	public Attribute(String[] att) {
		this.attributes = att;
		isNum = att[0].equals("num");
		name = att[1];
		pos = Integer.parseInt(att[2]);
		int len = att.length;
		if(att.length > 3){
			choices = new String[len - 3];
			System.arraycopy(att, 3, choices, 0, len - 3);
		}
	}

	public String getName() {
		return name;
	}

	
	
	public double getGain() {
		return gain;
	}

	public double computeGain(List<TrainData> list, Attribute CLASS) {
		// compute the gain

		// new an array to record the appear time
		int[][] freq = new int[this.nodeBranchNum()][CLASS.nodeBranchNum()];
		// an array for compute the entropy of the whole file
		int[] set = new int[CLASS.nodeBranchNum()];

		for (TrainData td : list) {
			String attribute = td.getAttributes()[this.pos];
			String label = td.getLabel();
			int pos = this.getBranch(attribute);
			if (pos == -1) {
				// this data is exception, ignore it
				continue;
			}
			freq[pos][CLASS.getBranch(label)] += 1; // add frequency
			set[CLASS.getBranch(label)] += 1;
		}
		// IG(A,S) = H(S) - \sum_{t \in T} p(t)H(t)
		double infoGain = Entropy.compute(set);
		// compute the Entropy for each attribute
		for (int i = 0; i < freq.length; i++) {
			infoGain -= (Entropy.sum(freq[i]) / Entropy.sum(set))
					* Entropy.compute(freq[i]);
		}
		this.gain = infoGain;
		return gain;
	}

	public int getPos() {
		return pos;
	}

	// find the branch that belongs to some data
	public int getBranch(String attribute) {
		// find which vals.equals this attribute
		for (int i = 0; i < choices.length; i++) {
			if (choices[i].toLowerCase().equals(attribute.toLowerCase())) {
				return i;
			}
		}
		System.out.println("	invalid data: " + attribute
				+ " does not belong to any vals");
		return -1; // didn't find the right branch
	}

	// how many branch should this node have?
	public int nodeBranchNum() {
		return choices.length;
	}

	@Override
	public FileReadable updateByString(String input) {
		return new Attribute(input.split(","));
	}

	@Override
	public Data copy() {
		return new Attribute(this.attributes);
	}

	@Override
	public String toString() {
		return "isNum+"+isNum +" | name:" + this.name +" | gain:"+this.gain;
	}

	public static void main(String[] args) {
//		// test
//		String line = "Type,0,student,doctor,professor,librarian,engineer";
//		Attribute a = (Attribute) new Attribute().updateByString(line);
//		System.out.println(a);
//		String line2 = "Label,6,C1,C2,C3,C4,C5";
//		Attribute CLASS = (Attribute) new Attribute().updateByString(line2);
//		
//		String file  = "trainProdSelection.arff";
//		FileRead<TrainData> fd = new FileRead<TrainData>();
//		List<TrainData> list = fd.readToClass(file,"@data",new TrainData());
//		System.out.println(a.computeGain(list,CLASS));
		
		
		//read from file 
		FileRead<Attribute> fr = new FileRead<Attribute>();
		List<Attribute> list = fr.readToClass("part_A_Attributtes.txt","",new Attribute());
		for(Attribute a : list){
			System.out.println(a);
		}
	}

	@Override
	public int compareTo(Object o) {
		Attribute a = (Attribute)o;
		if(this.getGain() > a.getGain()){
			return -1;
		}else if(this.getGain() < a.getGain()){
			return 1;
		}else{
			return 0;
		}
		
	}

}
