package team4.bean;

public class Entropy {
	public static void main(String[] args){
		int[] num ={2,3};
		System.out.println(compute(num));
	}
	
	
	public static double compute(int[] num){
		double p;
		double sum = sum(num);
		if(sum == 0){
			return 0;
		}
		
		double result = 0;
		for(int i:num){
			p = i/sum;
			if(p != 0){
				result -= (Math.log(p)/Math.log(2))*p;
			}
			
		}
		return result;
	}
	
	public static double sum(int[] num){
		int sum = 0;
		for(int i : num){
			sum += i;
		}
		return sum;
	}
	
}
