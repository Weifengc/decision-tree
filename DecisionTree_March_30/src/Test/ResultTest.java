package Test;

import java.util.List;

import team4.CategoryRules.MultipulSplit;
import team4.bean.Attribute;
import team4.bean.Rules;
import team4.bean.TestData;
import team4.fileIO.FileConvert;
import team4.fileIO.FileRead;
import ID3.DecisionTree;
import ID3.SplitMethod;

public class ResultTest {

	public static void main(String[] args){
		partA();
		//partB();
	}
	
	public static void partB(){
		String trainDataFile = "trainProdIntro.binary.arff";
		String attributesFile = "part_B_Attributtes.txt";
		String line2 = "notNum,Label,8,0,1";
		String testFile = "testProdIntro.binary.arff";
		
		Attribute CLASS = (Attribute) new Attribute().updateByString(line2);
		
		//get rules
		SplitMethod splitMethod = new MultipulSplit(CLASS,1);
		List<Rules> rules = splitMethod.returnRules(trainDataFile, attributesFile);
		
		//build tree
		DecisionTree t = new DecisionTree(trainDataFile,
				attributesFile, CLASS,rules);

		//use the rules to convert predict data
		FileRead<TestData> fr = new FileRead<TestData>();
		List<TestData> testList = fr.readToClass(testFile,"@data",new TestData());
		FileConvert<TestData> fc = new FileConvert<TestData>();
		List<TestData> testList2 = fc.changeToCategory(rules, testList);
		
		for(TestData td :testList2){
			System.out.println(t.predict(td));
		}
		
	}
	
	public static void partA(){
		String trainDataFile = "trainProdSelection.arff";
		String attributesFile = "part_A_Attributtes.txt";
		String line2 = "notNum,Label,6,C1,C2,C3,C4,C5";
		String testFile = "testProdSelection.arff";
		
		Attribute CLASS = (Attribute) new Attribute().updateByString(line2);
		
		//get rules
		SplitMethod splitMethod = new MultipulSplit(CLASS,2);
		List<Rules> rules = splitMethod.returnRules(trainDataFile, attributesFile);
		
		//build tree
		DecisionTree t = new DecisionTree(trainDataFile,
				attributesFile, CLASS,rules);

		//use the rules to convert predict data
		FileRead<TestData> fr = new FileRead<TestData>();
		List<TestData> testList = fr.readToClass(testFile,"@data",new TestData());
		FileConvert<TestData> fc = new FileConvert<TestData>();
		List<TestData> testList2 = fc.changeToCategory(rules, testList);
		
		for(TestData td :testList2){
			td.setLabel(t.predict(td));
			System.out.println(td);
		}
		
	}
	
}
